module demo_tb (
        input clk,
        input reset,
        input [7:0] din,
        output reg [7:0] dout
    );

    reg init = 1;

    demo DUT (
        .clk(clk),
        .reset(reset),
        .din(din),
        .dout(dout)
    );

    always @(posedge clk) begin
        if(init)
            assume(reset);
        if(!reset)
            assert(!dout[1:0]);
        init <= 0;
    end

endmodule