# LNL Academy 2020 - Digital Design

##	design structure
| folder | contents |
| ------ | ------ |
| bin | scripts and executables |
| doc | specification documents |
| lib | IP library |
| proj | project generation scripts |
| rtl | synthesizable model |
| sim | testbenches and simulation scripts |
| syn | synthesis scripts and constraint files |
| verif | verification files |

##   coding guidelines
*	**structure**
	*	use an identical directory structure for every project (see design structure above)
	*	use suffixes in file names that indicate the content of the file  
	`fifo_tb.sv`  
    `fifo_sim.tcl`
	*	use a single module, interface, program or package in a file
	*	use relative pathnames
	*	surround source files with **\`ifndef** and **\`endif** directives
	*	specify files required by the current file using the **\`include** directive
	*	use the **.sv** extension for files containing SystemVerilog source code
	*	use the **.v** extension for files containing Verilog-2001 source code
*	**style**
	*	put copyright notice, brief description, revision number, maintainer name at the beginning of each file
	*	use a trailer comment describing revision history for each source file
	*	lay out code for maximum readability and maintainability
	*	use indentation (4 spaces min.)
	*	preface each major section (e.g. always, class, package, etc.) with a comment describing why it exists and how it works
	*	use comments to describe the intent or functionality, not the behavior
	*	state design assumptions and known issues in the comments
	*	if necessary, include sections from the specification document and/or refer to it in the comments
	*	use `//` for any type of comment, use `/* ... */` for commenting out code segments
	*	delete bad code; do not comment-out bad code
	*	have only one declaration or statement per line
	*	break lines that are too long
	*	group port declarations that belong to the same interface or clock domain; do not group them by their directions
	*	declare constants/variables at the beginning of the module definition
	*	encapsulate repeatedly used operations in subroutines
	*	limit nesting levels of flow control statements (e.g. if-else) to 3
	*	do not enclose a single procedural assignment with begin-end; however, start it on a new line  
	`initial`  
    `   clk <= 0;`
	*	single assignments in a case statement can begin on the same line as the expression  
	`case(sel)`  
    `   0: data_out = data_in_0;`  
    `   default: data_out = data_in_1;`  
    `endcase`
	*	do not insert a new line before **begin**
	*	'end' keywords should be the only statement on a line  
	`always @(posedge clk) begin`  
    `   ...`  
	`end`
	*	add a space before and after every operator to improve readability  
	`assign result = num_0 + ((num_1 * num_2) % 3) / 4;`
	*	include a mechanism to enable/disable debug/verification statements  
	\`ifdef FORMAL  
    `   ...`  
    \`endif
*	**naming**
	*	give meaningful names to user-defined identifiers
	*	name objects according to function or purpose; avoid naming objects according to type or implementation
	*	use lowercase letters for all user-defined identifiers  
	`reg [7:0] led;`  
	`module adder`
	*	use UPPERCASE letters for constant identifiers  
	`parameter DATA_WIDTH = 8`
	*	separate words/numbers in identifiers using an underscore  
	`reg rd_en;`  
    `wire led_0;`
	*	do not use a mixed-case style  
	`reg [3:0] thisIdentifierIsHardToRead;`
	*	use the suffix **\_pkg** for packages  
	`axi_iface_pkg`
	*	use the suffix **\_n** for active low signals  
	`input reset_n`
	*	use the suffix **\_i** in module instance names or give the instance a more meaningful name  
	`fifo fifo_i`  
    `fifo header_fifo`
	*	if multiple instances of the same module are present, number the instance names  
    `fifo fifo_i_0`  	
    `fifo fifo_i_1`
	*	prefix signal names that connect to module ports with the module instance name  
	`wire fifo_i_0_empty;`
	*	do not use reserved words of popular languages as user-defined identifiers
	*	number bits in a vector using the range **[N:0]**  
	`reg [15:0] wr_ptr;`
	*	use the range **[N:0]** for packed array dimensions and **[0:N]** for unpacked array dimensions  
    `reg [7:0][3:0] mem [0:1][0:31];`
	*	do not specify a bit range when referring to a complete vector
	*	label generate blocks; prefix the label with **gen\_**  
	`for(...) begin: gen_receive_buffers`
	*	label closing 'end' keywords  
	`endcase: state`  
    `endcase // state`
	*	append the suffix **\_sync** to names of synchronization registers in CDC circuits, use **\_synced** for the final register in the chain
	*	when naming interface signals follow the rules below
	    *   <signal\_type\_or\_direction>\_<interface\_identifier>\_<signal_name>  
        `s_axis_tvalid`
        *   interface identifier can be omitted if it is included in the signal name  
        `s_aclk` ('a' stands for AXI)
        *   signal type/direction can be omitted if it is included in the signal name  
        `gmii_rxd`
	    *   if there are multiple interfaces of the same type add a user-defined identifier before the interface identifier  
        `m_udp_payload_axis_tdata`
*	**portability**
	*	avoid using **\`define** symbols which are global to the compilation
	*	minimize identifiers in shared name spaces
	*	use prefixes to differentiate identifiers in shared space
	*	assign variables from a single procedural block to prevent race conditions in simulation
	*	do not use the bitwise operators in a Boolean context (e.g. use && instead of & for Boolean 'AND')
	*	store shared data types and constants in packages which can then be imported into different scopes
	*	include attributes (e.g. ASYNC_REG) in the source code instead of adding them later via scripts
	*	use **this** when referring to data members (e.g. this.id) even if there is no ambiguity
	*	use named ports when instantiating modules or calling functions/tasks	
	*	prefer symbolic constants (e.g. parameter) over hardcoded numeric values
*	**design**
	*	use blocking assignments for combinational logic
	*	use non-blocking assignments for sequential logic
	*	use **always\_ff**, **always\_latch** or **always_comb** instead of **always**
	*	include a **default** case in every case statement
	*	initialize variables at declaration
	*	give an initial value to any variable that can store a value
	*	include a reset condition for counters, pointers, buffer full/empty logic, state machines, etc.
	*	reset condition must be the first statement in an if-else block so that dedicated pins on the flip-flops are used
	*	every design using an asynchronous reset should include a reset synchronizer circuit for each clock domain
	*	sensitivity lists should consist of either posedge/negedge clock/reset or * (always_comb)
