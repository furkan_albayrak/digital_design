module demo (
        input clk,
        input reset,
        input [7:0] din,
        output reg [7:0] dout
    );

    reg [7:0] buffer;
    reg [1:0] state;

    always @(posedge clk)
        if(reset) begin
            dout <= 0;
            state <= 0;
        end else
            case(state)
                0: begin
                    buffer <= din;
                    state <= 1;
                end
                1:
                    if(buffer[1:0])
                        buffer <= buffer + 1;
                    else
                        state <= 2;
                2: begin
                    dout <= dout + buffer;
                    state <= 0;
                end
            endcase

endmodule